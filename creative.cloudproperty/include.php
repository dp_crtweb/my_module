<?php

use Bitrix\Main\Event;
use \Creative\CloudProperty\Connectors\DropboxConnector;
use \Creative\CloudProperty\CloudService;
use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;
use Bitrix\Main\Config\Option;

//событие для того, чтобы другие модули могли подключить свой конектор
$event = new Event('creative.cloud_property', 'createCloudProperty');
$event->send();

//если в событии не подключен логер, то инстантим по умолчанию
if (!$connector = $event->getParameter('connector')) {
    $clientId = Option::get('creative.cloudproperty', 'clientId');
    $clientSecret = Option::get('creative.cloudproperty', 'clientSecret');
    $accessToken = Option::get('creative.cloudproperty', 'accessToken');

    $app = new DropboxApp($clientId, $clientSecret, $accessToken);
    $dropbox = new Dropbox($app);
    $connector = new DropboxConnector($app, $dropbox);
}

CloudService::getInstance()->setConnector($connector);

