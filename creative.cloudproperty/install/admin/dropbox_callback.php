<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use \Creative\CloudProperty\CloudService;
use Bitrix\Main\Loader;

Loader::includeModule("creative.cloudproperty");

$token = CloudService::getInstance()->auth();

CMain::FinalActions();
