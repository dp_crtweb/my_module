<?php

use Bitrix\Main\Application;
use Bitrix\Main\EventManager;

class creative_cloudproperty extends CModule
{
    public $MODULE_ID;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $PARTNER_NAME;
    public $PARTNER_URI;
    
    public function __construct()
    {
        
        if (file_exists(__DIR__ . "/version.php")) {
            
            $arModuleVersion = require_once(__DIR__ . "/version.php");
            $this->MODULE_ID = 'creative.cloudproperty';
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME = 'Облачное свойство';
            $this->MODULE_DESCRIPTION = 'добавляет возможность загружать из облака';
            $this->PARTNER_NAME = 'creative';
            $this->PARTNER_URI = 'http://crtweb.ru';
        }
    }
    
    function DoInstall()
    {
        $this->installEvents();
        $this->installFiles();
        RegisterModule($this->MODULE_ID);
    }
    
    function DoUnInstall()
    {
        $this->uninstallEvents();
        $this->unInstallFiles();
        UnRegisterModule($this->MODULE_ID);
    }
    
    //
    function installEvents()
    {
        $eventManager = EventManager::getInstance();
        foreach ($this->getEventsList() as $event) {
            $eventManager->registerEventHandlerCompatible(
                $event['FROM_MODULE_ID'],
                $event['EVENT_TYPE'],
                $this->MODULE_ID,
                $event['TO_CLASS'],
                $event['TO_METHOD'],
                $event['SORT']
            );
        }
    }
    
    function uninstallEvents()
    {
        $eventManager = EventManager::getInstance();
        foreach ($this->getEventsList() as $event) {
            $eventManager->unRegisterEventHandler(
                $event['FROM_MODULE_ID'],
                $event['EVENT_TYPE'],
                $this->MODULE_ID,
                $event['TO_CLASS'],
                $event['TO_METHOD']
            );
        }
    }
    
    /**
     * Возвращает список событий, которые должны быть установлены для данного модуля.
     *
     * @return array
     */
    protected function getEventsList()
    {
        return [
            [
                'FROM_MODULE_ID' => 'main',
                'EVENT_TYPE' => 'OnUserTypeBuildList',
                'TO_CLASS' => "\Creative\CloudProperty\Property",
                'TO_METHOD' => "getDescription",
                'SORT' => '1800',
            ],
            [
                'FROM_MODULE_ID' => 'iblock',
                'EVENT_TYPE' => 'OnIBlockPropertyBuildList',
                'TO_CLASS' => "\Creative\CloudProperty\Property",
                'TO_METHOD' => "getDescription",
                'SORT' => '1800',
            ],
        ];
    }
    
    
    function installFiles()
    {
        CopyDirFiles(
            $this->getInstallatorPath() . '/admin/',
            Application::getDocumentRoot() . '/bitrix/admin/'
        );
    }
    
    function unInstallFiles()
    {
        DeleteDirFiles(
            $this->getInstallatorPath() . '/admin/',
            Application::getDocumentRoot() . '/bitrix/admin/'
        );
    }
}

