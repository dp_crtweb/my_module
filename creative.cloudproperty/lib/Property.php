<?php

namespace Creative\CloudProperty;

use \Creative\CloudProperty\File\File;
use Bitrix\Main\Loader;

Loader::includeModule("creative.cloudproperty");

class Property
{
    function getDescription()
    {
        return [
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "Cloud",
            "DESCRIPTION" => "Файл в облаке",
            "PrepareSettings" => ["\Creative\CloudProperty\Property", "prepareSettings"],
            "GetSettingsHTML" => ["\Creative\CloudProperty\Property", "getSettingsHTML"],
            "GetPropertyFieldHtml" => ["\Creative\CloudProperty\Property", "getPropertyFieldHtml"],
            "GetPropertyFieldHtmlMulty" => ["\Creative\CloudProperty\Property", "getPropertyFieldHtmlMulty"],
            "ConvertToDB" => ["\Creative\CloudProperty\Property", "convertToDB"],
            "GetAdminListViewHTML" => ["\Creative\CloudProperty\Property", "getAdminListViewHTML"],
        ];
    }
    
    function convertToDB($arProperty, $value)
    {
        if (
            $value['VALUE']['DEL'] == "Y" &&
            isset($value['VALUE']['CURRENT']) &&
            CloudService::getInstance()->check($value['VALUE']['CURRENT'])
        ) {
            CloudService::getInstance()->delete($value['VALUE']['CURRENT']);
        } elseif (is_array($value['VALUE']['FileTest'])) {
            if (isset($value['VALUE']['CURRENT']) && CloudService::getInstance()->check($value['VALUE']['CURRENT'])) {
                
                CloudService::getInstance()->delete($value['VALUE']['CURRENT']);
            }
            $file = new File($value['VALUE']['FileTest']);
            $value["VALUE"] = CloudService::getInstance()->save($file);
        }
        
        return $value;
    }
    
    function getPropertyFieldHtmlMulty($arProperty, $value, $strHTMLControlName)
    {
        return self::makeInput($arProperty, $value, $strHTMLControlName);
    }
    
    function getPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        return self::makeInput($arProperty, $value, $strHTMLControlName);
    }
    
    function prepareSettings($arFields)
    {
        return $arFields["USER_TYPE_SETTINGS"];
    }
    
    function getAdminListViewHTML($arProperty, $value, $strHTMLControlName)
    {
        return $value["VALUE"];
    }
    
    function getSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        return '';
    }
    
    private static function makeInput($arProperty, $value, $strHTMLControlName)
    {
        $link = false;
        if ($value['VALUE'] && $value['VALUE'] != '1' && CloudService::getInstance()->check($value['VALUE'])) {
            $link = CloudService::getInstance()->get($value['VALUE']);
        }
        
        if ($link) {
            $html = "<a href='{$link}'>{$value['VALUE']}</a>
                <p>
                    <span class='adm-input-file'>
                        <span>Изменить файл</span>
                        <input type='file' name='{$strHTMLControlName["VALUE"]}[FILE]' class='adm-designed-file'>
                    </span>
                </p>
                <p>
                    <label>
                        <input type='hidden' name='{$strHTMLControlName["VALUE"]}[CURRENT]' value='{$value["VALUE"]}'>
                        <input type='checkbox' name='" . $strHTMLControlName["VALUE"] . "[DEL]' value='Y'>
                        Удалить
                    </label>
                </p>";
        } else {
            $html = "<span class='adm-input-file'>
                    <span>Добавить файл</span>
                    <input type='file' name='{$strHTMLControlName["VALUE"]}[FILE]' value='{$value["VALUE"]}' class='adm-designed-file'>
                  </span>";
            
        }
        
        return $html;
    }
}