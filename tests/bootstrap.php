<?

require dirname(__DIR__) . '/vendor/autoload.php';
require __DIR__ . '/stubs/Application_stub.php';
require __DIR__ . '/stubs/Connector_stub.php';
require __DIR__ . '/stubs/Dropbox_app_stub.php';
require __DIR__ . '/stubs/Dropbox_file_stub.php';
require __DIR__ . '/stubs/Dropbox_stub.php';
require __DIR__ . '/stubs/Option_stub.php';
