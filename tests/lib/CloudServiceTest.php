<?php

use Creative\CloudProperty\CloudService;
use Creative\CloudProperty\File\File;

class CloudServiceTest extends \PHPUnit_Framework_TestCase
{
    protected $service;
    
    public function testAuth()
    {
        $this->assertSame(
            $this->service->auth(),
            'http://test.loc'
        );
    }
    
    public function testGet()
    {
        $this->assertSame(
            $this->service->get(),
            'filename'
        );
    }
    
    public function testSave()
    {
        $file = new File();
        $this->assertSame(
            $this->service->save($file),
            'filename'
        );
    }
    
    public function testCheck()
    {
        $this->assertSame(
            $this->service->check(),
            true
        );
    }
    
    public function testDelete()
    {
        $this->assertSame(
            $this->service->delete(),
            true
        );
    }
    
    public function setUp()
    {
        $connector = new Connector_stub();
        $this->service = CloudService::getInstance();
        $this->service->setConnector($connector);
    }
}