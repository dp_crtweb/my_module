<?php
use Creative\CloudProperty\Connectors\DropboxConnector;
use Bitrix\Main\Application;
use Creative\CloudProperty\File\File;

class DropboxConnectorTest extends \PHPUnit_Framework_TestCase
{
    
    protected $connector;

    function testAuth()
    {
        $this->assertSame(
            'link_to_dropbox',
            $this->connector->auth()
        );
    }
    
    function testCallbackPage()
    {
        Application::getInstance()->setParams('code','stage');

        $this->assertSame(
            'my_token',
            $this->connector->auth()
        );
    }
    
    function testSave()
    {
        $file_path = dirname(dirname(__DIR__)).'/stubs/test_file.txt';
        $file = new File();
        $file->makeFromFile($file_path);
        
        $this->assertSame(
            'file_name_for_test',
            $this->connector->save($file)
        );
    }
    
    function testGet()
    {
        $this->assertSame(
            'TemporaryLink',
            $this->connector->get('test')
        );
        
    }
    
    function testCheck()
    {
        $this->assertSame(
            true,
            $this->connector->check('test')
        );
    }
    
    function testDelete()
    {
        $this->assertSame(
            true,
            $this->connector->delete('test')
        );
    }
    
    
    public function setUp()
    {
        $app = new DropboxApp();
        $dropbox = new Dropbox();
        $this->connector = new DropboxConnector($app, $dropbox);
    }
}