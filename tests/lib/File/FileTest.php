<?php

use Creative\CloudProperty\File\File;

class FileTest extends \PHPUnit_Framework_TestCase
{
    protected $file;
    
    function testConstructor(){
    
        $array = [
            'name' => 'test',
            'type' => 'text',
            'tmp_name' => 'test.txt',
            'size ' => '0',
        ];
        $file = new File($array);
        
        $this->assertSame(
            true,
            $file->isInit()
        );
    }
    
    function testBadConstructor(){
        $file = new File();
    
        $this->assertSame(
            false,
            $file->isInit()
        );
    }
    
    function testMakeFromFile(){
        $file = new File();
        $file->makeFromFile($this->file);
        
        $this->assertSame(
            true,
            $file->isInit()
        );
    }
    
    function testMakeFromBitrix(){
        $array = [
            'ID' => '2',
            'TIMESTAMP_X' => '13.12.2017 16:31:10',
            'MODULE_ID' => 'iblock',
            'HEIGHT' => '198',
            'WIDTH' => '358',
            'FILE_SIZE' => '18169',
            'CONTENT_TYPE' => 'image/jpeg',
            'SUBDIR' => 'iblock/4c5',
            'FILE_NAME' => '4c5107460cdb2b02364e2dc1c4427b5b.jpg',
            'ORIGINAL_NAME' => 'image-news-card-2.jpg',
            'DESCRIPTION' =>'',
            'HANDLER_ID' =>'',
            'EXTERNAL_ID' => '9d43fdaf006b604584e4c079552ac47e',
            'SRC' => '/upload/iblock/4c5/4c5107460cdb2b02364e2dc1c4427b5b.jpg',
        ];
        $file = new File();
        $file->makeFromBitrix($array);
    
        $this->assertSame(
            true,
            $file->isInit()
        );
    }
    
    function testBadMakeFromBitrix(){
        $array = [
            'ID' => '2',
            'TIMESTAMP_X' => '13.12.2017 16:31:10',
            'MODULE_ID' => 'iblock',
            'HEIGHT' => '198',
            'WIDTH' => '358',
            'FILE_SIZE' => '18169',
            'CONTENT_TYPE' => 'image/jpeg',
            'SUBDIR' => 'iblock/4c5',
            'FILE_NAME' => '4c5107460cdb2b02364e2dc1c4427b5b.jpg',
            'DESCRIPTION' =>'',
            'HANDLER_ID' =>'',
            'EXTERNAL_ID' => '9d43fdaf006b604584e4c079552ac47e',
            'SRC' => '/upload/iblock/4c5/4c5107460cdb2b02364e2dc1c4427b5b.jpg',
        ];
        try{
            $file = new File();
            $file->makeFromBitrix($array);
        } catch (\Exception $e){
        
        }
        $this->assertSame(
            false,
            $file->isInit()
        );
    }
    
    
    function testGetName(){
        $file = new File();
        $file->makeFromFile($this->file);
    
        $this->assertStringEndsWith('txt', $file->getName());
    }
    function testGetPath(){
        $file = new File();
        $file->makeFromFile($this->file);
    
        $this->assertSame(
            $this->file,
            $file->getPath()
        );
    }
    function testGetSize(){
        $file = new File();
        $file->makeFromFile($this->file);
    
        $this->assertSame(
            0,
            $file->getSize()
        );
    }
    function testGetType(){
        $file = new File();
        $file->makeFromFile($this->file);
    
        $this->assertSame(
            'inode/x-empty',
            $file->getType()
        );
    }
    
    
    public function setUp()
    {
        $this->file = dirname(dirname(__DIR__)).'/stubs/test_file.txt';
    }
}