<?php

namespace Bitrix\Main;

class Application{
    
    private static $instance = null;
    
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    protected $code,$state;
    
    public function getContext(){
        return $this;
    }
    public function getRequest(){
        return $this;
    }
    
    public function getQuery($code)
    {
        switch ($code){
            case 'code':
                return $this->code;
                break;
            case 'state':
                return $this->state;
                break;
        }
    }
    
    public function setParams($code, $state)
    {
        $this->code = $code;
        $this->state = $state;
    }
}
