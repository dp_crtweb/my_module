<?php

use Creative\CloudProperty\Connectors\ConnectorInterface;
use Creative\CloudProperty\File\File;

class Connector_stub implements ConnectorInterface{
    public function get($file_id){
        return 'filename';
    }
    public function save(File $file){
        return 'filename';
    }
    public function check($file_id){
        return true;
    }
    public function delete($file_id){
        return true;
    }
    public function auth(){
        return 'http://test.loc';
    }
}