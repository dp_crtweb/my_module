<?php

class DropboxApp
{
    protected $clientId, $clientSecret, $accessToken;
    
    public function __construct($clientId, $clientSecret, $accessToken)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->accessToken = $accessToken;
    }
}