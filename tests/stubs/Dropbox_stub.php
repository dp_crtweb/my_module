<?php

class Dropbox
{
    protected $app;
    
    public function __construct($app)
    {
        $this->app = $app;
    }
    
    public function getAuthHelper()
    {
        return $this;
    }
    
    public function getAuthUrl()
    {
        return 'link_to_dropbox';
    }
    
    public function getAccessToken(){
        return $this;
    }
    
    public function getToken()
    {
        return 'my_token';
    }
    
    public function upload(){
        return $this;
    }
    
    public function getPathLower()
    {
        return 'file_name_for_test';
    }
    
    public function getTemporaryLink(){
        return $this;
    }
    public function getLink(){
        return 'TemporaryLink';
    }
    
    public function getMetadata(){
        return true;
    }
    
    public function delete(){
        return true;
    }
}